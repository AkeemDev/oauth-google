<?php

namespace App\Controller;

use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use League\OAuth2\Client\Provider\Google;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class UserGoogleController extends AbstractController
{
    /**
     * @Route("/connect/", name="")
     * @param Request $request
     * @return Response
     */
    public function connectCheckAction(Request $request, SessionInterface $session)
    {

        $provider = new Google([
            'clientId'     => '274575402155-1c6915k6amnosk0n0dlcdhtq1sftt7ck.apps.googleusercontent.com',
            'clientSecret' => 'qtVTvxhl1sbtDiJpws06rxui',
            'redirectUri'  => 'http://localhost:8000/home',
        ]);
        dd($provider);


        return $this->render('home/index.html.twig');
    }
}
