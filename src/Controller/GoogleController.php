<?php

namespace App\Controller;

use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GoogleController extends AbstractController
{
    /**
 * @Route("/google", name="google")
 */
    public function index(ClientRegistry $clientRegistry): Response
    {
        return  $clientRegistry
            -> getClient ( 'google' ) // clé utilisée dans config / packages / knpu_oauth2_client.yaml
            -> redirect ([
                 'email',
                'https://www.googleapis.com/auth/youtube.readonly',
                'https://www.googleapis.com/auth/calendar'
            ]);
    }
    /**
     * @Route("/connect", name="connect_google_check")
     */
    public function connectCheckAction (Request $request, ClientRegistry $clientRegistry): Response
    {
        $client = $clientRegistry->getClient('google');

            try {
                $accessToken = $client->getAccessToken();
                $user = $client->fetchUserFromToken($accessToken);

                // do something with all this new power!
                // e.g. $name = $user->getFirstName();
                var_dump($user); die("a la fin !!!!");
                // ...
            } catch (IdentityProviderException $e) {
                // something went wrong!
                // probably you should return the reason to the user
                var_dump($e->getMessage());
        }
    }
}
